void horizEq() {
  for(int i = 0; i <= 6; i++) {
    int myval = max(0, (msgeq7.getValue(i) - 100) / 50);
    for(int j = 0; j < numLedRows; j++) {
     if(j <= myval) {
       setPixelCoords(i+1, j-1, makeColor(180-myval*30, 100, 50));
     } else {
       setPixelCoords(i+1, j-1, makeColor(0, 0, 0));
     }
    }
  }
  delay(50);
}  
