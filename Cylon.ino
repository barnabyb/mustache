int cylon_initTime = 0;
int cylon_colour = 0;
int cylon_loopTime = 10000000;
int cylon_fadeOff = 0;


void Cylon() {
  if(cylon_initTime = 0) {
    cylon_initTime = micros();
  }
  int peak = ((micros() - cylon_initTime) % cylon_loopTime);
  float peakRemainder = (float)peak / cylon_loopTime; // 0-1
  int adjustedWidth = W() + 2*cylon_fadeOff;
  int roughPeak = int(peakRemainder * adjustedWidth + 0.5); // -N-N
  int roughMin = roughPeak - cylon_fadeOff - 2; // -N-N
  int roughMax = roughPeak + cylon_fadeOff + 2; // -N-N
  
  for(int i = roughMin; i <= roughMax; i++) {
    float point_on_curve = (float)(i)/(adjustedWidth); // 0-1
    float curve_value = abs(peakRemainder-point_on_curve)*127*adjustedWidth/(cylon_fadeOff + 1);
    int curve;
    if(curve_value > 127 || curve_value < 0) {
      curve = 0;
    } else {
      curve = cos_wave[int(curve_value)];
    }
    int adjusted_i = i - cylon_fadeOff;
    if(adjusted_i >= 0 && adjusted_i < W()) {
      drawColumn(adjusted_i, makeColor(cylon_colour, 50, int(50*((float)curve/256))));
    }
  }
  if(roughMin > 0) {
    drawColumn(roughMin - 1, 0);
  }
  if(roughMin >= W()) {
    cylon_colour = random(360);
    cylon_initTime = micros();
  }
  
  delay(50);
}
