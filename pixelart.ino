boolean logo[20][10] = {
  {0,0,0,0,0,0,0,0,0,0},
  {0,0,0,0,1,1,0,0,0,0},
  {0,1,0,1,0,0,1,0,1,0},
  {0,1,0,0,1,1,0,0,1,0},
  {0,0,1,0,0,0,0,1,0,0},
  {0,0,1,0,0,0,0,1,0,0},
  {0,0,1,0,0,0,0,1,0,0},
  {0,0,0,1,0,0,1,0,0,0},
  {0,0,0,1,0,0,1,0,0,0},
  {0,0,0,1,0,0,1,0,0,0},
  {0,0,0,1,0,0,1,0,0,0},
  {0,0,0,1,0,0,1,0,0,0},
  {0,0,0,1,0,0,1,0,0,0},
  {0,0,0,1,0,0,1,0,0,0},
  {0,0,1,0,0,0,0,1,0,0},
  {0,0,1,0,0,0,0,1,0,0},
  {0,1,0,0,0,0,0,0,1,0},
  {0,1,0,0,0,0,0,0,1,0},
  {0,0,0,0,0,0,0,0,0,0}
};

boolean squarething[4][4] = {
  {1,1,1,1},
  {1,0,0,1},
  {1,0,0,1},
  {1,1,1,1}
};

boolean animatedSquare[2][4][4] = {
  {
    {1,1,1,1},
    {1,0,0,1},
    {1,0,0,1},
    {1,1,1,1}
  },
  {
    {0,0,0,0},
    {0,1,1,0},
    {0,1,1,0},
    {0,0,0,0}
  }
};

void pixelArt() {
  // Here we need to translate from array coords to our grid coords
  int size_x = 10;
  int size_y = 10;
  int start_x = 0;
  int start_y = 3;
  int colour = makeColour(random(360), 100, 50);
  for(int y = 0; y < size_y; y++) {
    for(int x = 0; x < size_x; x++) {
      if(logo[x][y]) {
        setPixelCoords(start_x + x, start_y - y, colour);
      } else {
        setPixelCoords(start_x + x, start_y - y, 0);
      }
    }
  }
  for(int i = 0; i < 255; i++) {
    arrayFade(2);
  }
  delay(250);
}

      
    
