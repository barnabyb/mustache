#include <MSGEQ7.h>

#include <OctoWS2811.h>




const int ledsPerStrip = 50;

const int numLedRows = 5;
const int numLedColumns = 10;
boolean debug = true;

// draw modes

#define DRAW_NORMAL 0
#define DRAW_INVERT_H 1
#define DRAW_INVERT_V 2
#define DRAW_INVERT_BOTH 3

int drawMode = DRAW_NORMAL;
int rotateMode = 0;


int modeSelect;
int const modeChangeChance = 0.1;
int const modeChangeInterval = 60000000;
int lastModeCheck;
int lastModeChange;
int const numModes = 1;




DMAMEM int displayMemory[ledsPerStrip*6];
int drawingMemory[ledsPerStrip*6];

const int config = WS2811_GRB | WS2811_800kHz;

OctoWS2811 leds(ledsPerStrip, displayMemory, drawingMemory, config);

MSGEQ7 msgeq7;

uint8_t const cos_wave[128] PROGMEM = 
{255,255,255,255,254,254,253,253,
252,252,251,250,249,248,247,246,
245,243,242,241,239,238,236,234,
232,231,229,227,225,223,221,218,
216,214,212,209,207,204,202,199,
197,194,191,189,186,183,180,177,
174,171,168,165,162,159,156,153,
150,147,144,141,138,135,131,128,
125,122,119,116,113,109,106,103,
100,97,94,91,88,85,82,79,
76,73,71,68,65,62,60,57,
54,52,49,47,45,42,40,38,
35,33,31,29,27,25,23,22,
20,18,17,15,14,12,11,10,
9,8,6,6,5,4,3,2,
2,1,1,1,0,0,0,0};


void setup( ) {
  pinMode(1, OUTPUT);
  digitalWrite(1, HIGH);
  digitalWrite(1, LOW);
  leds.begin();
  Serial.begin(9600);
  modeSelect = 0;
  lastModeCheck = micros();
  lastModeChange = lastModeCheck;
  
  msgeq7.init(A4, 10, 9); // Initialize Output, Strobe, and Reset Pins
}

void loop() {
  msgeq7.poll();
  int now = micros();
  if(now - lastModeCheck < modeChangeInterval) {
    //modeSelect = randomModeChange();
    lastModeCheck = now;
  }
  switch(modeSelect) {
    case 0:
      pixelArt();
  }
  leds.show();
  // Don't delay in this loop. Let specific vis functions delay per their particular smoothing requirements.
}


void randomModeChange() {
  int loops_since_change = (micros() - lastModeChange) % modeChangeInterval;
  if(random(1000000) > (modeChangeChance ^ modeChangeInterval)*1000000) {
    int new_mode = random(numModes);
    Debug("Changing mode to " + String(new_mode));
    modeSelect = new_mode;
  } else {
    Debug("Not changing mode.");
  }
}

int ledIndex(int x, int y) {
  // let's assume our coordinate are on the standard plane, with (0, 0) in the lower left. 
  // Convert it to an ledIndex for the octows2811 library, which may be wired up in a number of ways.
  boolean rowLeftToRight = true; // 0 = L-R, 1 = R-L
  boolean altRowLeftToRight = true; // true = 0-5, 6-10, 11-15, ... | false = 0-5, 10-6, 11-15, ...'
  boolean bottomToTop = true; 
  
  // This function comes into play after drawMode/rotateMode, so is unaffected and uses the base values.
  
  int rowBase;
  
  if(bottomToTop) {
    rowBase = y * numLedColumns; 
  } else {
    rowBase = (numLedColumns) * (numLedRows - y - 1);
  }
  if(rowLeftToRight) {
    if(y % 2 == 0 || altRowLeftToRight) {
      return rowBase + x;
    } else {
      return rowBase + numLedColumns - x - 1;
    }
  } else if (y % 2 == 0 || !altRowLeftToRight) {
    return rowBase + numLedColumns - x - 1;
  } else {
    return rowBase + x;
  }
}  

void setPixelCoords(int x, int y, int color) {
  if(rotateMode == 90) {
    // too drunk to do this math right now
  }
  if(drawMode == DRAW_INVERT_H || drawMode == DRAW_INVERT_BOTH) {
    x = W() - x - 1;
  }
  if(drawMode == DRAW_INVERT_V || drawMode == DRAW_INVERT_BOTH) {
    y = H() - y - 1;
  }
  leds.setPixel(ledIndex(x, y), color);
}

void drawColumn(int x, int color) {
  for(int i = 0; i < numLedRows; i++) {
    setPixelCoords(i, x, color);
  }
}

void drawRow(int y, int color) {
  for(int i = 0; i < numLedColumns; i++) {
    setPixelCoords(y, i, color);
  }
}

void simpleDrawTest() {
  for (int j = 0; j < numLedRows; j++) {
  for (int i = 0; i < numLedColumns; i++) {
    setPixelCoords(i, j, makeColor(0, 100, 50));
    leds.show();
    delay(500);
    setPixelCoords(i, j, makeColor(0, 0, 0));
  }
  }
}

void Debug(String whatever) {
  if(debug == true) {
    Serial.println(whatever);
  }
}

int W() {
  if(rotateMode == 90 || rotateMode == 270) {
    return numLedRows;
  } else {
    return numLedColumns;
  }
}

int H() {
  if(rotateMode == 90 || rotateMode == 270) {
    return numLedColumns;
  } else {
    return numLedRows;
  }
}

void arrayFade(int fade) {
  for(int i = 0; i < (numLedRows * numLedColumns); i++) {
    unsigned int oldcolor = leds.getPixel(i);
    int r = colorFade((oldcolor & 0x00FF0000) >> 16, fade);
    int g = colorFade((oldcolor & 0x0000FF00) >> 8, fade);
    int b = colorFade((oldcolor & 0x000000FF), fade);
    unsigned int newcolor = (r << 16) | (g << 8) | b;
    //Debug("oc: " + String(oldcolor) + "   nc: " + String(newcolor));
    leds.setPixel(i, (r << 16) | (g << 8) | b);
  }
  leds.show();
}
    
int colorFade(int strength, int fade) {
  return max(strength - fade, 0);
}
  
    
